package com.academiasmoviles.lab01edad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {

            var edad = edtEdad.text.toString().toInt()

            if (edad <= 17 )
            {
                tvResultado.text = "Usted es MENOR de edad"
            }
            else
            {
                tvResultado.text = "Usted es MAYOR de edad"
            }



        }

    }
}